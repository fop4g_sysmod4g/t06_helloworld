
#include "SFML/Graphics.hpp"

using namespace sf;

/*
SFML super skinny skeleton starter app
*/
int main()
{
	// Create the main window
	RenderWindow window(VideoMode(1200, 800), "Hello world!");

	// Start the game loop
	while (window.isOpen())
	{
		// Process events
		Event event;
		while (window.pollEvent(event))
		{
			// Close window: exit
			if (event.type == Event::Closed)
				window.close();
		} 


		// Clear screen
		window.clear();

		// Update the window
		window.display();
	}

	return EXIT_SUCCESS;
}
